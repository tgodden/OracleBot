# OracleBot
# Copyright (C) 2017  Tom Godden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import discord
import asyncio
import sys
from oraclebot.oracle_bot_commands import commands, evaluate_expression
from oraclebot.parsing.parser import parse_message
from oraclebot.db.database import Database
from oraclebot.common.strings import generate_insult

# ----- settings -----
greet_users = False
# --------------------

client = discord.Client()
dbs = {}


def get_db(message):
    """Return the database that belongs to the channel the message
    was received on.

    The channel ID of the message is used as key in the dictionary.

    Args:
        message: a discord.Message

    Returns:
        the corresponding Database
    """

    key = message.channel.id
    return dbs.get(key)


@client.event
async def on_ready():
    """Load all channels and create or load a Database for each connected
    text channel. Add the members of these channels to the database.

    This procedure is called when the program is fully set up and connected
    to the server.
    """

    print("Starting")
    for channel in client.get_all_channels():
        if (channel.type == discord.ChannelType.text):
            db = Database("sqlite:///data/" + str(channel.id) + ".db")
            dbs[channel.id] = db
            for member in channel.guild.members:
                db.add_user(member)
    print('Logged in as', client.user.name, client.user.id)
    print('------')


@client.event
async def on_message(message):
    """Handle a received message.

    Empty messages are ignored. Messages starting with "=" are considered
    expressions and passed on to be evaluated as such. Other expression
    are parsed by the normal parsed. The resulting command is then
    executed and the result is sent to the corresponding Channel.

    Args:
        message: a discord.Message
    """

    if len(message.content) < 1:  # Empty message
        return
    if message.content[0] == "=":  # Expression
        await evaluate_expression(client, message)
    else:
        parsed = parse_message(message.content)
        command = commands.get_command(parsed[0])
        if len(parsed) > 1:
            args = parsed[1:]
        else:
            args = []
        if command != None:
            db = get_db(message)
            if command.can_do(message.author, bot_mode=db.bot_mode):
                await command.do(client, message, db, args)
            else:
                await message.channel.send("You are not allowed to do that!")


@client.event
async def on_member_update(before, after):
    """Handle a user status change.

    Currently only handling offline users coming online. Easy to extend.

    Args:
        before: the user's status before
        after: the user's status right now
    """
    # User comes online
    if greet_users and (before.status == discord.Status.offline) and (after.status == discord.Status.online):
        channel = after.server.default_channel
        db = get_db(after)
        if db.add_user(after):
            await client.send_message(channel, "Welcome " + after.display_name + "!")
        else:
            await client.send_message(channel, "Welcome back " + after.display_name + "!")


def main(argv):
    """Starts the program

    Args:
        argv: the system arguments passed on.
            This should only include the bot token.
    """
    try:
        token = argv[1]
    except IndexError:
        print("ERROR: you need to pass bot token as argument")
        sys.exit(-1)
    client.run(token)


if __name__ == "__main__":
    main(sys.argv)
