# OracleBot
# Copyright (C) 2017  Tom Godden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from sqlalchemy import create_engine, Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

Base = declarative_base()


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    active_character_id = Column(Integer)

    characters = relationship("Character")

    def __repr__(self):
        return "id: " + str(self.id) + "\t|name: " + self.name


class Character(Base):
    __tablename__ = "characters"

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("users.id"))

    user = relationship("User", back_populates="characters")
    traits = relationship("CharacterTrait")
    items = relationship("Item")

    def __repr__(self):
        return str(self.id) + "[" + str(self.user.name) + "]"


class Trait(Base):
    __tablename__ = "traits"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)

    def __repr__(self):
        return self.name


class CharacterTrait(Base):
    __tablename__ = "character_traits"

    id = Column(Integer, primary_key=True)
    character_id = Column(Integer, ForeignKey("characters.id"))
    trait_id = Column(Integer, ForeignKey("traits.id"))
    value = Column(String)

    character = relationship("Character", back_populates="traits")
    trait = relationship("Trait")

    def __repr__(self):
        return self.trait.name + "\t: " + self.value


class Item(Base):
    __tablename__ = "items"

    id = Column(Integer, primary_key=True)
    character_id = Column(Integer, ForeignKey("characters.id"))

    character = relationship("Character", back_populates="items")
    traits = relationship("ItemTrait")

    def __repr__(self):
        return str(self.id) + "[" + str(self.character_id) + "]"


class ItemTrait(Base):
    __tablename__ = "item_traits"

    id = Column(Integer, primary_key=True)
    item_id = Column(Integer, ForeignKey("items.id"))
    trait_id = Column(Integer, ForeignKey("traits.id"))
    value = Column(String)

    item = relationship("Item", back_populates="traits")
    trait = relationship("Trait")

    def __repr__(self):
        return self.trait.name + "\t: " + self.value
