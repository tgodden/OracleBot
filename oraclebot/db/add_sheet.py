# OracleBot
# Copyright (C) 2017  Tom Godden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import csv


def add_character_from_file(db, datafile, user):
    """Generates a character from a csv and add it and its traits to the database,
    returning its ID.

    Args:
        db: database to store the character in
        datafile: the csv containing the data
        user: the user calling the command

    Returns:
        the character id of the newly created character
    """
    char_id = db.add_character(user)
    reader = csv.DictReader(datafile)
    for row in reader:
        key = row["Trait"].lower()
        value = row["Value"]
        db.add_new_trait(key)
        db.add_trait_to_char(key, char_id, value)
    return char_id


def add_item_from_file(db, datafile, character_id):
    """Generates an item from a csv and add it and its traits to the database,
    returning its ID.

    Args:
        db: database to store the item in
        datafile: the csv containing the data
        character_id: the character_id the item is assigned to. Can be None

    Returns:
        the item id of the newly created item
    """
    reader = csv.DictReader(datafile)
    item_traits = {row["Trait"].lower(): row["Value"] for row in reader}
    return db.add_item(item_traits, character_id)
