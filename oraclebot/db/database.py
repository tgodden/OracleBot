# OracleBot
# Copyright (C) 2017  Tom Godden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from oraclebot.db.sheet_db import *
from oraclebot.common.bot_mode import BotMode
from collections import OrderedDict
import sqlalchemy as sa


class Database:
    """The Database class.

    The Database stores the current bot mode and character data and recognizes
    users.

    A Database should be created for each channel that wants to use
    the bot.
    """

    def __init__(self, url):
        """Database constructor

        Creates or accesses the actual database.

        Args:
            url: the url of the database
        """
        self.bot_mode = BotMode.default
        self.engine = sa.create_engine(url, echo=False)
        self.Session_class = sa.orm.sessionmaker(bind=self.engine)
        self.session = self.Session_class()
        User.metadata.create_all(self.engine)

    # ----- Conversion to useful formats -----#
    @staticmethod
    def convert_user_row(row):
        return {"id": row.id,
                "name": row.name,
                "active_character_id": row.active_character_id
                }

    @staticmethod
    def convert_character_row(row):
        return {"id": row.id,
                "user": row.user.name,
                "user_id": row.user_id,
                "string": str(row),
                }

    @staticmethod
    def convert_trait_row(row):
        return {"id": row.id,
                "name": row.name,
                }

    @staticmethod
    def convert_item_trait_table(table):
        res = OrderedDict()
        for row in table:
            res[row.trait.name] = row.value
        return res

    @staticmethod
    def convert_character_trait_table(table):
        res = OrderedDict()
        for row in table:
            res[row.trait.name] = row.value
        return res

    # ----- Database operations -----#

    def add_user(self, user):
        """Add a user to the database.

        Args:
            user: the discord.User to add

        Returns:
            false when the user is already in the database,
            the user's id in the database when the user isn't
        """
        try:
            u = User(id=int(user.id), name=user.display_name)
            self.session.add(u)
            self.session.commit()
            return u.id
        except sa.exc.IntegrityError:
            self.session.rollback()
            return False

    def get_all_users(self):
        """Get a list of all users.

        Returns:
            A dict containing all users and data about them
        """
        users = self.session.query(User).all()
        return [self.convert_user_row(user) for user in users]

    def get_user(self, discord_id):
        """Get a user by its discord ID.

        A user is returned as a dictionary, which knows the following keys:
            "id"
            "name"
            "active_character_id"

        Args:
            discord_id: the discord ID of the user

        Returns:
            A dict containing information about the user
            If the user is not in the database, return None
        """
        result = self.session.query(User).filter(
            User.id == int(discord_id)).first()
        try:
            return self.convert_user_row(result)
        except AttributeError:
            return None

    def get_characters(self, discord_user=False):
        """Get a list of all characters. If a user is passed, return only the user's
           characters.

        A character is returned as a dictionary, which knows the following keys:
            "id"
            "user"
            "user_id"
            "string"

        Args:
            discord_user=False: the discord user whose characters to return

        Returns:
            a list of dicts containing character information
        """
        if discord_user:
            result = self.session.query(Character).filter(
                Character.user_id == int(discord_user.id)).all()
        else:
            result = self.session.query(Character).all()
        return [self.convert_character_row(char) for char in result]

    def add_character(self, discord_user):
        """Add a new character to the database.

        Args:
            discord_user: the discord.User creating a character

        Returns:
            the character ID of the newly created character
        """
        char = Character(user_id=int(discord_user.id))
        self.session.add(char)
        self.session.commit()
        return char.id

    def set_active_character(self, discord_user, character_id):
        """Change the active character for a user.

        Args:
            discord_user: the discord.User changing his active character
            character_id: the character ID of the character to switch to
        """
        user = self.session.query(User).filter(
            User.id == discord_user.id).first()
        user.active_character_id = int(character_id)
        self.session.commit()

    def add_new_trait(self, trait_name):
        """Add a new trait to the database.

        Args:
            trait_name: the name for the new trait

        Returns:
            the ID of the newly created trait
            returns False if there already is a trait with this name
        """
        try:
            trait = Trait(name=trait_name)
            self.session.add(trait)
            self.session.commit()
            return trait.id
        except sa.exc.IntegrityError:
            self.session.rollback()
            return False

    def get_trait(self, trait_name):
        """Get a dictionary with data about a trait by name.

        The dictionary knows the following keys:
            "id"
            "name"

        Args:
            trait_name: the name of the trait

        Returns:
            a dictionary with data about a trait
        """
        trait = self.session.query(Trait).filter(
            Trait.name == trait_name).first()
        return self.convert_trait_row(trait)

    def add_trait_to_char(self, trait_name, character_id, value):
        """Add a trait to a character and give it a value.

        Args:
            trait_name: the name of the trait
            character_id: ID of the character to give the trait to
            value: value of the trait
        """
        trait_id = self.get_trait(trait_name)["id"]
        self.session.add(CharacterTrait(
            character_id=character_id, trait_id=trait_id, value=value))
        self.session.commit()

    def get_character_traits(self, character_id):
        """Return all traits of a character.

        Args:
            character_id: ID of the character to get the traits from

        Returns:
            A dictionary with as key the trait names and as value the
            trait values.
        """
        ctraits = self.session.query(CharacterTrait). \
            filter(CharacterTrait.character_id == character_id).order_by(
            CharacterTrait.trait_id).all()
        return self.convert_character_trait_table(ctraits)

    def add_trait_to_item(self, item_id, trait_name, value):
        """Add a trait to an item and give it a value.

        Args:
            trait_name: the name of the trait
            item_id: ID of the character to give the trait to
            value: value of the trait
        """
        try:
            trait_id = self.get_trait(trait_name)["id"]
        except AttributeError:
            trait_id = self.add_new_trait(trait_name)
        self.session.add(ItemTrait(
            item_id=item_id,
            trait_id=trait_id,
            value=value))
        self.session.commit()

    def add_item(self, item_traits={}, character_id=None):
        """Add an item to the database.

        Args:
            item_traits={}: a dictionary with keys being trait names and values
                            being the values of the trait
            character_id=None: the character to assign the item to
        """
        item = Item(character_id=character_id)
        self.session.add(item)
        self.session.commit()
        for key, value in item_traits.items():
            self.add_trait_to_item(item.id, key, value)
        return item.id

    def get_item_traits(self, item_id):
        """Return all traits of an item.

        Args:
            item_id: ID of the item to get the traits from

        Returns:
            A dictionary with as key the trait names and as value the
            trait values.
        """
        itraits = self.session.query(ItemTrait). \
            filter(ItemTrait.item_id == item_id).all()
        return self.convert_item_trait_table(itraits)

    def assign_item(self, item_id, char_id):
        """Assign an item to a character

        Args:
            item_id: the ID of the item
            char_id: ID of the character to assign the item to
        """
        item = self.session.query(Item). \
            filter(Item.id == item_id).one()
        item.character_id = char_id
        self.session.commit()

    def get_character_item_ids(self, char_id):
        """Returns a list of all item ID's currently assigned to the character

        Args:
            char_id: ID of the character

        Returns:
            a list of all item ID's currently assigned to the character
        """
        items = self.session.query(Item). \
            filter(Item.character_id == char_id).all()
        return [item.id for item in items]
