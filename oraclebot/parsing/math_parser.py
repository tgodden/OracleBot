# OracleBot
# Copyright (C) 2017  Tom Godden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from random import seed, random
import numbers
seed()


class ParseException(Exception):
    pass


class UnclosedBracketException(ParseException):
    pass


class Node():
    """The main Node class. Should never be created directly!"""

    def __init__(self):
        """Initializes fields that are the same for all Node subclasses. Should
        only be called as super constructor, never directly!"""
        self.dice = []

    def evaluate():
        """A plain Node object should never be created directly, and subclasses
        should override this procedure, meaning this should never be called.

        Raises:
            NotImplementedError"""
        raise NotImplementedError


class BranchNode(Node):
    """A node of the parse tree"""

    def __init__(self, value, left=None, right=None):
        """BranchNode constructor

        Args:
            value: the value of the node
            left: the left child
            right: the right child
        """
        super().__init__()
        self.left = left
        self.right = right
        self.value = self._get_operation(value)

    def _get_operation(self, op):
        """Get an operation based on the string of the operator.

        Possible operators:
            Arithmetic operators: +, -, /, *, x

        x is the implicit multiplication, acting differently dan * when rolling
        dice. E.g. 3*d10 rolls a d10 and multiplies it by 3, while 3xd10 rolls
        3 d10's and returns their sum

        Args:
            op: the string indicating the operator

        Returns:
            a function applying the operator to two arguments
        """
        def minus_function(a, b):
            if a is None:
                return -b.evaluate()
            else:
                return a.evaluate()-b.evaluate()

        def implicit_multiplication_function(a, b):
            if isinstance(b, DieRollLeafNode):
                return b.evaluate(int(a.evaluate()))
            else:
                return a.evaluate()*b.evaluate()
        operations = {
                "+": lambda a, b: a.evaluate()+b.evaluate(),
                "-": minus_function,
                "/": lambda a, b: a.evaluate()/b.evaluate(),
                "*": lambda a, b: a.evaluate()*b.evaluate(),
                "x": implicit_multiplication_function,
                }
        return(operations[op])

    def evaluate(self):
        """Evaluates the branch and returns the result.

        Apply the operator to the evaluation of the left and right branch.

        The values of dice rolls will be backpropagated through the tree.

        Returns:
            the value of the evaluation
        """
        val = self.value(self.left, self.right)
        if self.left is not None:
            self.dice += self.left.dice
        if self.right is not None:
            self.dice += self.right.dice
        return val

    def __str__(self):
        """String representation for a BranchNode"""
        return "(" + str(self.left) + str(self.value) + str(self.right) + ")"


class LeafNode(Node):
    """A main LeafNode class. Should never be created directtly!"""

    def __init__(self):
        """Calls Node init. A LeafNode constructor should never be called
        outside of the LeafNode subclass constructors"""
        super().__init__()


class DieRollLeafNode(LeafNode):
    """A LeafNode representing a random roll of a die"""

    def __init__(self, sidestring):
        """DieRollLeafNode constructor.

        Args:
            sidestring: String describing the die roll. Formatted as e.g. "d20".
        """
        super().__init__()
        self.sides = float(sidestring[1:])
        self.value = False

    def evaluate(self, amount_of_dice=1):
        """Evaluate the roll of the die and returns the value.

        Caches the result for consistency should it be evaluated again later.

        Returns:
            the result of the roll
        """
        if not self.value:
            self.value = 0
            self.dice = []
            for i in range(amount_of_dice):
                roll = int(random() * int(self.sides)) + 1
                self.value += roll
                self.dice += [roll]
        return self.value


class ValueLeafNode(LeafNode):
    """A LeafNode representing a numeric value"""

    def __init__(self, valuestring):
        """ValueLeafNode constructor.

        Args:
            valuestring: the string containing the value, will be cast to a float
        """
        super().__init__()
        self.value = float(valuestring)

    def evaluate(self):
        """Returns the value stored in the node.

        Returns:
            the value stored in the node
        """
        return self.value


class _Grouper():
    """The Grouper class groups expressions by either brackets or
    dice roll expressions. A group takes the form of a list consisting
    out of a mixture of numbers, operators, or other groups.

    The grouper uses a bracket_stack value to count the nesting depth
    of a place in the expression. The Grouper.group function is called
    recursively until the depth is 0.

    There are two types of groups: an arithmetic group (stored in
    current_group) and a die_group (stored in die_group).
    """

    def __init__(self, expression):
        self.expression = expression
        self.groups = []
        self.current_group = ""
        self.die_group = ""
        self.bracket_stack = 0
        self.procs = self._create_dict()
        self._make_groups()

    def _end_group_implicitly(self):
        """Ends the group, adding the implicit multiplication.

        This procedure is called at the beginning of a group, e.g:
            3d10 -> 3xd10
            3(4+1) -> 3x(4+1)
        """
        if(self.die_group != ""):
            self.groups.append(self.die_group)
            self.die_group = ""
            self.groups.append("x")
        elif(self.current_group != ""):
            self.groups.append(self.current_group)
            self.current_group = ""
            self.groups.append("x")

    def _end_group_explicitly(self, operator):
        """Ends the group, adding the operator.
        """
        if(self.die_group != ""):
            self.groups.append(self.die_group)
            self.die_group = ""
        elif(self.current_group != ""):
            self.groups.append(self.current_group)
            self.current_group = ""
        self.groups.append(operator)

    def _finish_parsing(self):
        """Cleans up leftover characters.
        """
        if self.bracket_stack != 0:
            raise UnclosedBracketException
        if self.current_group != "":
            self.groups.append(self.current_group)
        elif self.die_group != "":
            self.groups.append(self.die_group)

    def _check_brackets(self, c):
        """Checks the brackets

        Args:
            c: the current character

        Returns:
            whether or not all brackets are closed
        """
        if self.bracket_stack == 0:
            return True
        else:
            self.current_group += c
            return False

    #----- Operation per character -----#
    def _left_bracket(self, c):  # (
        """Executed when a '(' is read.

        Args:
            c: the current character, not used

        Increases the bracket_stack by 1 and ends a group implicitly.
        """
        self.bracket_stack += 1
        self._end_group_implicitly()  # End group, maybe add an implicit multiplication

    def _right_bracket(self, c):  # )
        """Executed when a ')' is read.

        Reduces the bracket stack by 1. Then, if the bracket_stack is 0,
        end the group and send it to the grouper to be grouped recursively.

        Args:
            c: the current character, not used

        Throws:
            UnclosedBracketException when there are more )'s than ('s
        """
        self.bracket_stack -= 1
        if(self.bracket_stack < 0):  # More right brackets than left
            raise UnclosedBracketException
        if(self.bracket_stack == 0):  # Completely unbracketed
            self.groups.append(
                _Grouper(self.current_group+self.die_group).groups)
            self.current_group = ""
            self.die_group = ""

    def _operator(self, c):  # + - * /
        """Executed when either '+', '-', '*' or '/' is read.

        If the bracket stack is 0, end the group and add the operator

        Args:
            c: the current character
        """
        if self._check_brackets(c):
            self._end_group_explicitly(c)  # End group and add an operator

    def _dice(self, c):  # d D
        """Executed when a 'd' or 'D' is read.

        If the bracket stack is 0, end the group and at "d" to the dice group.

        Args:
            c: the current character
        """
        if self._check_brackets(c):
            self._end_group_implicitly()  # End group, maybe add an implicit multiplication
            self.die_group += "d"

    def _number(self, c):  # 0-9 .
        """Executed when 0-9 or a '.' is read.

        Adds the number to the currently active group type.

        Args:
            c: the current character
        """
        if self.die_group == "":  # Not working on a die
            self.current_group += c
        else:  # Working on a die
            self.die_group += c

    def _space(self, c):
        """Executed when a space is read.

        Does nothing.

        Args:
            c: the current character, not used
        """
        pass

    def _not_in_dict(self, c):
        """Executed when an invalid character is read.

         Always throws a ParseException.

        Throws:
            ParseException: always
        """
        raise ParseException
    #----- The Dictionary -----#

    def _create_dict(self):
        """Create the dictionary containing the possible operators.

        Returns:
            the dictionary containing the operators
        """
        d = {
            "(": self._left_bracket,
            ")": self._right_bracket,
            "d": self._dice,
            " ": self._space,
            }
        d.update(dict.fromkeys(["+", "-", "*", "/"], self._operator))
        d.update(dict.fromkeys(
            [str(m) for m in list(range(0, 10))] + ["."], self._number))
        return d

    def _get_from_dict(self, c):
        """Get the function from the dict, corresponding to the current character

        Args:
            c: the current character

        Returns:
            the corresponding function

        Throws:
            ParseException when the character is not in the dict
        """
        try:
            return self.procs[c]
        except KeyError:
            return self._not_in_dict(c)

    def _make_groups(self):
        """
        Convert the expression to an array (group),
        consisting of operators, numbers and other groups.

        Parts of the expression in brackets will be converted
        to another group. This nesting can happen multiple times.
        """
        for c in self.expression:
            self._get_from_dict(c)(c)
            # END OF PARSING LOOP
        self._finish_parsing()


class Tree():
    """The Tree class describes the parse tree for an expression."""

    def __init__(self, expression):
        """Tree constructor

        Args:
            expression: the expression to be parsed
        """
        self.expression = expression
        self.root = self._parse()
        self._evaluated = False

    def _make_groups(self, expression):
        """Group the expression.

        Args:
            expression: the expression to be parsed

        Returns:
            the grouped expression
        """
        groups = _Grouper(expression).groups
        return groups

    def _parse_groups(self, groups):
        """
        Parse the groups, recursively looping through their tree structure and
        converting it to a tree that can be evaluated.

        Args:
            groups: a grouped expression

        Returns:
            the parsed expression

        Throws:
            IndexError
        """
        if(len(groups) > 1):  # Is a list of items
            # Search for + and -
            i = len(groups) - 1
            while (i >= 0) and ((groups[i] != "+") and (groups[i] != "-")):
                i -= 1
            if i < 0:  # No + or - found
                # Search for * and / and x
                i = len(groups) - 1
                while (i >= 0)\
                        and (groups[i] != "*")\
                        and (groups[i] != "/")\
                        and (groups[i] != "x"):
                    i -= 1
            # An operator should have been found, so create root
            if(i >= 0):
                left = self._parse_groups(groups[:i])
                right = self._parse_groups(groups[i+1:])
                op = groups[i]
                return BranchNode(value=op, left=left, right=right)
            else:
                print(groups)
                raise IndexError(i)
        elif(len(groups) == 1):  # Is a single element, not empty
            element = groups[0]
            if isinstance(element, list):  # Is between ( )
                return self._parse_groups(element)
            else:
                return DieRollLeafNode(element) if element[0].lower() == "d"\
                        else ValueLeafNode(element)

    def _parse(self):
        """Do the grouping and parsing of grouping.

        Returns:
            the root node of the parsed tree
        """
        if self.expression == "":
            raise ParseException
        root = self._parse_groups(self._make_groups(self.expression))
        return root

    def evaluate(self):
        """Evaluate the tree.

        This also sets the _evaluated value to True.

        Returns:
            the evaluation of the tree
        """
        self._evaluated = True
        return self.root.evaluate()

    def get_dice_rolls(self):
        """Get the dice rolled during evaluation.

        Evaluates the tree when if is not yet evaluated.

        Returns:
            a list of dice rolls
        """
        if not self._evaluated:
            self.evaluate()
        return self.root.dice
