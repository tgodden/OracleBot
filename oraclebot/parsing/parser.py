# OracleBot
# Copyright (C) 2017  Tom Godden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re


def parse_message(message_string):
    """Parse a message, by splitting it on spaces.

    Args:
        message_string: the message received

    Returns:
        the list containing the split message
    """
    matches = re.split("[ ]", message_string)
    matches = list(filter(None, matches))
    matches = [m.strip() for m in matches]
    return matches


def format_message(list_of_tuples, separator=" : ", header=()):
    """Put a list of tuples into a nice ascii table.

    Args:
        list_of_tuples: the data to put into the table
        separators: the separator between columns
        header: the header of the table

    Returns:
        a string containing the formatted table with data
    """
    lengths = []
    for tup in list_of_tuples + [header]:
        for i in range(len(tup)):
            try:
                lengths[i] = max(len(tup[i]), lengths[i])
            except IndexError:
                lengths += [len(tup[i])]
    text = "```"
    if len(header) > 0:
        heading = [header,
                   tuple("-"*l for l in lengths)]
    else:
        heading = []
    for tup in heading + list_of_tuples:
        first = True
        for i in range(len(tup)):
            s = tup[i]
            l = lengths[i]
            if first:
                first = False
            else:
                text += separator
            text += s + " "*(l - len(s))
        text += "\n"
    text += "```"
    return text
