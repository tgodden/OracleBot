# OracleBot
# Copyright (C) 2017  Tom Godden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import oraclebot.parsing.math_parser as mp
from oraclebot.parsing.parser import format_message
from oraclebot.common.commands import Command, CommandList
from oraclebot.common.bot_mode import BotMode
from oraclebot.common.get_remote_file import get_file
import oraclebot.common.strings as strings
from oraclebot.db.add_sheet import add_character_from_file, add_item_from_file
import asyncio
from random import random, seed

seed()

# ----- Roles ----- #
admin = {"admin"}
dm = {"dm"}
player = {"player"}
timed_out = {"jail"}


def get_role_by_name(server, role_name):
    """Get a discord.Role by its name.

    Args:
        server: the discord.Server containing the roles
        role_name: the name of the discord.Role

    Returns:
        the corresponding discord.Role
    """
    for r in server.roles:
        if r.name == role_name:
            return r


# ----- Commands ----- #


async def help_command_f(client, message, db, args):
    """Posts the help message.

    Args:
        client: discord.Client of the server
        message: discord.Message received, containing the author and channel
        db: oraclebot.db.Database of the channel
        args: optional arguments, does nothing
    """
    await message.channel.send(commands.list_commands(message.author, bot_mode=db.bot_mode))


async def source_command_f(client, message, db, args):
    """Posts the source message.

    Args:
        client: discord.Client of the server
        message: discord.Message received, containing the channel
        db: oraclebot.db.Database of the channel
        args: optional arguments, does nothing
     """
    await message.channel.send(strings.source_string())


async def d_command_f(client, message, db, args):
    """Roll a die.

    Args:
        client: discord.Client of the server
        message: discord.Message received, containing the channel
        db: oraclebot.db.Database of the channel
        args: args[0]: number of sides of the die
    """
    try:
        d = int(args[0])
        await message.channel.send(int(random() * d) + 1)
    except ValueError:
        await message.channel.send(strings.d_error_string())
    except IndexError:
        await message.channel.send(strings.d_error_string())


async def insult_command_f(client, message, db, args):
    """Generate and post an insult.

    Args:
        client: discord.Client of the server
        message: discord.Message received, containing the channel and author
        db: oraclebot.db.Database of the channel
        args: target of the insult
    """
    author = message.author
    name = author.mention
    if len(args) > 0:
        # joined because they are split when parsing args
        target = " ".join(args)
        await message.channel.send(strings.insult_string(name, target))
    else:
        await message.channel.send(strings.insult_error_string(author.mention))


async def timeout_command_f(client, message, db, args):
    """Timeout a user.

    The timed_out variable should be configured to include the name of the timed
    out role(s) of the discord server. The user is given the role for a certain
    amount of time, after which the user gets its original roles back.

    Args:
        client: discord.Client of the server
        message: discord.Message received, containing the channel
        db: oraclebot.db.Database of the channel
        args: arg[0]: target to be timed out
              arg[1]: time out duration

    """
    if len(args) != 2:
        await message.channel.send(strings.arg_error_string(2, message.author.mention))
        return
    target = message.guild.get_member_named(args[0])
    if target is None:
        try:
            target = message.guild.get_member(message.mentions[0].id)
        except IndexError:
            await message.channel.send(strings.no_such_user_error_string(message.author.mention))
            return
    time = int(args[1])
    old_roles = target.roles
    jail_list = [get_role_by_name(message.guild, r) for r in timed_out]
    await target.edit(roles=jail_list)
    await message.channel.send(strings.timeout_start_string(target.mention, time))
    await asyncio.sleep(time)
    await target.edit(roles=old_roles)
    await message.channel.send(strings.timeout_end_string(target.mention, time))


# ----- RPG commands ----- #
async def create_character_f(client, message, db, args):
    """Create a character from a csv file.

    Args:
        client: discord.Client of the server
        message: discord.Message received, containing the channel
        db: oraclebot.db.Database of the channel
        args: arg[0]: the file
    """
    if len(message.attachments) != 1:
        await message.channel.send(strings.no_file_error_string(message.author.mention))
        return
    url = message.attachments[0]["url"]
    f = get_file(url)
    char_id = add_character_from_file(db, f, message.author)
    f.close()


async def create_item_f(client, message, db, args):
    """Create an item from a csv file.

    Args:
        client: discord.Client of the server
        message: discord.Message received, containing the channel
        db: oraclebot.db.Database of the channel
        args: arg[0]: the file
    """
    if len(message.attachments) != 1:
        await message.channel.send(strings.no_file_error_string(message.author.mention))
        return
    url = message.attachments[0]["url"]
    f = get_file(url)
    char_id = db.get_user(message.author.id)["active_character_id"]
    item_id = add_item_from_file(db, f, char_id)
    f.close()


async def set_active_character_f(client, message, db, args):
    """Select an active character for a user.

    Args:
        client: discord.Client of the server
        message: discord.Message received, containing the channel
        db: oraclebot.db.Database of the channel
        args: arg[0]: character ID
    """
    char_id = args[0]
    db.set_active_character(message.author, char_id)
    await message.channel.send(strings.set_active_character_string(message.author.mention, char_id))


async def get_sheet_f(client, message, db, args):
    """Post the character sheet of the user's active character.

   Args:
        client: discord.Client of the server
        message: discord.Message received, containing the channel
        db: oraclebot.db.Database of the channel
        args: optional arguments, not used
    """
    char_id = db.get_user(message.author.id)["active_character_id"]
    result = [(key.capitalize(), value)
              for key, value in db.get_character_traits(char_id).items()]
    await message.channel.send(format_message(result))


async def list_characters_f(client, message, db, args):
    """List characters.

    Args:
        client: discord.Client of the server
        message: discord.Message received, containing the channel
        db: oraclebot.db.Database of the channel
        args: discord user requesting character list. If this is false, lists all
              characters.
    """
    if len(args) == 1:
        raise IndexError
    chars = db.get_characters(*args)
    result = []
    for char in chars:
        traits = db.get_character_traits(char["id"])
        result += [(char["string"], traits["name"])]
    await message.channel.send(format_message(result))


async def list_items_f(client, message, db, args):
    """Lists the items of the user's active character.

    Args:
        client: discord.Client of the server
        message: discord.Message received, containing the channel
        db: oraclebot.db.Database of the channel
        args: discord user requesting items
    """

    char_id = db.get_user(message.author.id)["active_character_id"]
    ids = db.get_character_item_ids(char_id)
    items = [[(key.capitalize(), value) for key, value in
              db.get_item_traits(i).items()] for i in ids]
    item_strings = "\n".join([format_message(item) for item in items])
    await message.channel.send(item_strings)


async def list_my_characters_f(client, message, db, args):
    """List only user's characters.

    Args:
        client: discord.Client of the server
        message: discord.Message received, containing the channel
        db: oraclebot.db.Database of the channel
        args: discord user requesting character list.
    """
    await list_characters_f(client, message, db, [message.author])


async def set_bot_mode(client, message, db, args):
    """Set the bot mode.

    Args:
        client: discord.Client of the server
        message: discord.Message received, containing the channel
        db: oraclebot.db.Database of the channel
        args: args[0]: name of the bot mode
    """
    if len(args) != 1:
        await message.channel.send(strings.arg_error_string(1, message.author.mention))
        return
    mode = args[0]
    try:
        new_mode = BotMode[mode]
        db.bot_mode = new_mode
        await message.channel.send(strings.set_bot_mode_string(mode))
    except KeyError:
        await message.channel.send(strings.set_bot_mode_error_string(BotMode.__members__.keys()))


# ------------------------ #

"""List of commands that can be parsed.

The oraclebot.common.command.Command is added to a dict in the
oraclebot.common.command.CommandList constructor.
"""
commands = CommandList([
    ("!help", Command(help_command_f, description="display this message")),
    ("!d", Command(d_command_f,
                   args=["sides"],
                   description="throw a die")),
    ("!insult", Command(insult_command_f, visible=False)),
    ("!source", Command(source_command_f, description="get source code")),
    ("!timeout", Command(timeout_command_f, description="time out a user",
                         args=["user", "seconds"], roles=admin)),
    ("!newchar", Command(create_character_f,
                         description="create a new character", bot_mode=BotMode.rpg, roles=player)),
    ("!newitem", Command(create_item_f,
                         description="create a new item", bot_mode=BotMode.rpg, roles=player)),
    ("!myitems", Command(list_items_f,
                         description="list your active items", bot_mode=BotMode.rpg, roles=player)),
    ("!allchars", Command(list_characters_f,
                          description="get a list of characters", bot_mode=BotMode.rpg, roles=player)),
    ("!mychars", Command(list_my_characters_f,
                         description="get a list of all your characters", bot_mode=BotMode.rpg, roles=player)),
    ("!selectchar", Command(set_active_character_f, description="select a character to play with",
                            args=["character_id"], bot_mode=BotMode.rpg, roles=player)),
    ("!sheet", Command(get_sheet_f, description="show current character sheet",
                       bot_mode=BotMode.rpg, roles=player)),
    ("!mode", Command(set_bot_mode,
                      description="set chat mode", args=["mode"], roles=admin)),
])


# ------ Evaluation of expressions ----- #


async def evaluate_expression(client, message):
    """Parse expressions (messages starting with '=') and post the result in
    the discord.Channel.

    Args:
        client: discord.Client of the server
        message: the received discord.Message
    """
    expression = message.content[1:]
    try:
        tree = mp.Tree(expression)
        value = tree.evaluate()
        dice = [str(d) for d in tree.get_dice_rolls()]
        if dice:
            string_value = str(int(value)) + " (" + ", ".join(dice) + ")"
        else:
            string_value = str(value)
        await message.channel.send(message.author.mention + ": " + string_value)
    except(mp.ParseException, ValueError):
        await message.channel.send(strings.expression_error_string(message.author.mention))
