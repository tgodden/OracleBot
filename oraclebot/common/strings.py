from random import random, gauss, seed

seed()
"""These functions generate strings that are used as reply to users"""


def generate_insult():
    """Generate an insult.

    Insults are randomly generated from a list of adjectives and insults, with
    the amount of adjectives chosen according to a gauss curve.

    Returns:
        A string containing a randomly generated insult
    """
    adjectives = ["bloody", "stinking", "disgusting", "frog-eating", "feeding",
                  "brainless", "disloyal", "garlic-breathed", ]
    insults = ["dimwit", "numbskull", "Frenchman", "dolt", "oaf",
               "rapscallion", "cockroach", "maggot", "trashpanda", ]
    amount_of_adjectives = int(max(-1, gauss(0, 2))) + 1
    adjective_string = ""
    for i in range(amount_of_adjectives):
        adjective_string += adjectives[int(len(adjectives) * random())]
        if (i < amount_of_adjectives - 1):
            adjective_string += ","
        adjective_string += " "

    chosen_insult = insults[int(len(insults) * random())]
    return adjective_string + chosen_insult


def source_string():
    """Return the string containing a link to the source code

    Returns:
        a string containing a link to the source code
    """
    return "Source code is available at https://gitlab.com/tgodden/OracleBot"


def d_error_string():
    """Return a string insulting the user to pass a number of sides for a die

    Returns:
        an insult to the user, telling him to pass a number
    """
    return "Pass a number you " + generate_insult() + "!"


def insult_string(name, target):
    """Returns a string, saying that the caller thinks the target is a
       randomly generated insult

    Args:
        name: name of the caller
        target: name of the target
    Returns:
        a string, saying that the caller thinks the target is a randomly
        generated insult
    """
    return name + " thinks " + target + " is a " + generate_insult() + "!"


def insult_error_string(name):
    """Returns a string insulting the caller for not calling the insult command
       correctly.

       Args:
            name: name of the caller
       Returns:
           a string insulting the caller for not calling the insult command
           correctly.
    """
    return "Get your shit together, " + name + "!"


def arg_error_string(nr_args, name):
    """Returns a string insulting the caller for not calling the command with the
    correct number of arguments.

        Args:
            nr_args: correct number of arguments
            name: name of the caller
        Returns:
            a string insulting the caller for not using the correct amount of arguments
    """
    return "This command needs " + str(nr_args) + " arguments. Learn to count " + name + "!"


def no_file_error_string(name):
    """Returns a string insulting the caller for not adding a file with their message

        Args:
            name: name of the caller
        Returns:
            a string insulting the caller for not adding a file with their message
    """
    return "Maybe you should actually pass a file " + name + "?"


def no_such_user_error_string(name):
    """Returns a string insulting the caller for choosing an invalid user

        Args:
            name: name of the caller
        Returns:
            a string insulting the caller for choosing an invalid user
    """
    return "That person only exists in your head, " + name + "!"


def timeout_start_string(target, time):
    """Returns a string saying the target is being timed out for a certain
       amount of time

    Args:
        target: the target to be timed out
        time: the time the target needs to be timed out for

    Returns:
        a string saying the target is being timed out for a certain
        amount of time
    """
    return "Timed out " + target + " for " + str(time) + " seconds."


def timeout_end_string(target, time):
    """Returns a string indicating the target is no longer timed out.

    Args:
        target: the target that was timed out
        time: the time the target was timed out for

    Returns:
        a string indicating the target is no longer timed out
    """
    return str(time) + " second timeout for " + target + " ended."


def set_active_character_string(mention, char_id):
    """Returns a string saying the user has set their active character
       to the passed character id

    Args:
        mention: the user's name or mention
        char_id: the character of the character the user wants to set his active
                 character to

    Returns:
        a string saying the user has set their active character
        to the passed character id
    """
    return "Set " + mention + " active character to character " + char_id + "."


def set_bot_mode_string(mode):
    """Returns a string indicating the bot mode has been switched

    Args:
        mode: the name of the new bot mode

    Returns:
        a string indicating the bot mode has been switched
    """
    return "Switched to " + mode + " mode."


def set_bot_mode_error_string(modes):
    """Returns a string indicating the mode was invalid, followed by a list of
       available modes.

    Args:
        modes: the list of possible modes

    Returns:
        A string indicating the mode was invalid, followed by a list of available
        modes.
    """
    return "Invalid mode. Possible modes are " + ", ".join(modes) + "."


# ----- Expressions -----#


def expression_error_string(mention):
    """Returns a string indicating there was an error parsing the expression.

    Args:
        mention: name or mention of the user passing the expression

    Returns:
        a string indicating there was an error parsing the expression
    """
    return "Didn't understand that, " + mention + "!"
