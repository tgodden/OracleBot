# OracleBot
# Copyright (C) 2017  Tom Godden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from oraclebot.common.bot_mode import BotMode
from oraclebot.parsing.parser import format_message


class CommandList:
    """A collection of commands"""

    def __init__(self, string_command_pairs=[]):
        """CommandList constructor

        The list of commands is put into a dict.

        Args:
            string_command_pairs=[]: a list of pairs. The first element of the
                                     pair indicates the string used to identify
                                     the command, the second element the command
                                     itself.
        """
        self.dict = {}
        self.add_commands(string_command_pairs)

    def get_command(self, command_string):
        """Return a command by its string.

        Args:
            command_string: the string used to identify the command

        Returns:
            corresponding command
        """
        return self.dict.get(command_string)

    def add_command(self, command_string, command):
        """Add a command to the CommandList

        Args:
            command_string: the string used to identify the command
            command: the command
        """
        self.dict[command_string] = command

    def add_commands(self, string_command_pairs):
        """Add a list of commands to the CommandList.

        Args:
            string_command_pairs: a list of pairs. The first element of the pair
                                  indicates the string used to identify the
                                  command, the second element the command itself.
         """
        for string, command in string_command_pairs:
            self.add_command(string, command)

    def list_commands(self, member, bot_mode=BotMode.default):
        """Create a list of commands that the user can currently execute.

        This takes into account the rank of the user and the current bot mode.

        Args:
            member: the discord.Member calling the command
            bot_mode=BotMode.default: the current bot mode

        Returns:
            a string containing each command the user can currently execute,
            with information about the command.
        """
        result = []
        for key, value in sorted(self.dict.items()):
            if value.visible and value.can_do(member, bot_mode=bot_mode):
                result += [(key, " ".join(value.args), value.description)]
        return format_message(result, header=("Command", "Arguments", "Description"))


class Command:
    """Command class"""

    def __init__(self, to_execute, args=[], description="No description",
                 visible=True, bot_mode=BotMode.default, roles={"@everyone"}):
        """Command constructor

        Args:
            to_execute: the function that needs to be executed when the command
                        is called.
            args=[]: an optional list of strings indicating the arguments
                     the function takes to the user
            description="No description": a description of the function for the user
            visible=True: wether or not the command should be shown with the !help
                          command
            bot_mode=BotMode.default: the bot mode this command should be active in
            roles={"@everyone"}: the names of the discord.Roles that are allowed
                                 to call this command

        Note: the to_execute function should take four arguments:
                client: the server's discord.Client
                message: the discord.Message calling the command
                db: the database for the channel
                args: a list of optional arguments
        """
        self.do = to_execute
        self.args = args
        self.description = description
        self.visible = visible
        self.bot_mode = bot_mode
        self.roles = roles

    def can_do(self, member, bot_mode=BotMode.default):
        """Check wether or not a member is allowed to call the command

        Args:
            member: the member trying to call the command
            bot_mode=BotMode.default: the current bot mode

        Returns:
            boolean indicating wether or not the member is allowed to call
        """
        member_roles = {r.name for r in member.roles}
        return (not self.roles.isdisjoint(member_roles)) and\
            ((self.bot_mode == BotMode.default) or (self.bot_mode == bot_mode))
