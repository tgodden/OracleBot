# OracleBot
# Copyright (C) 2017  Tom Godden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import requests
import tempfile


def get_file(url):
    """Get a file by url.

    Args:
        url: the url of the file

    Returns:
        the requested file
    """
    r = requests.get(url)
    f = tempfile.TemporaryFile(mode="w+t")
    f.write(r.text)
    f.seek(0)
    return f
