# OracleBot
# Copyright (C) 2017  Tom Godden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from oraclebot.parsing import parser as p


class ParserTest(unittest.TestCase):

    def testFormatter(self):
        test_lot = [("abc", "", "zyxwvu"),
                    ("", "test", ""),
                    (),
                    ("1", "string", "", "908"),
                    (""),
                    ("1"),
                    ]
        check = "```abc :        : zyxwvu\n    : test   :       \n\n1   : string :        : 908\n\n1  \n```"
        formatted = p.format_message(test_lot)
        print(repr(formatted))
        self.assertEqual(formatted, check)
