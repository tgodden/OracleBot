# OracleBot
# Copyright (C) 2017  Tom Godden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import sys
from oraclebot.parsing import math_parser as mp


class ParserTest(unittest.TestCase):

    def test_parsing(self):
        with self.assertRaises(mp.ParseException):
            mp.Tree("")._parse()

    def test_unclosed_bracket(self):
        with self.assertRaises(mp.UnclosedBracketException):
            mp.Tree(")")._parse()
        with self.assertRaises(mp.UnclosedBracketException):
            mp.Tree("(")._parse()
        with self.assertRaises(mp.UnclosedBracketException):
            mp.Tree(")((7+39)-(9138)")._parse()

    def test_unknown(self):
        with self.assertRaises(mp.ParseException):
            mp.Tree("thisisnotinthedict").__parse()
        with self.assertRaises(mp.ParseException):
            mp.Tree("5+bull").__parse()


class EvaluatorTest(unittest.TestCase):

    def test_negative(self):
        self.assertEqual(mp.Tree("-1").evaluate(), -1.0)

    def test_float(self):
        self.assertEqual(mp.Tree("1.5").evaluate(), 1.5)

    def test_evaluation(self):
        self.assertEqual(mp.Tree("9 - 3 * 4+ 5+1").evaluate(), 3.0)
        self.assertEqual(mp.Tree("9-3*(4+5)+1").evaluate(), -17.0)
        self.assertEqual(mp.Tree("-1.5").evaluate(), -1.5)

    def test_implicit_multipilcation(self):
        self.assertEqual(mp.Tree("9(10)").evaluate(), 90)
        self.assertEqual(mp.Tree("23d1").evaluate(), 23)
        self.assertEqual(mp.Tree("d1d1").evaluate(), 1)

    def test_brackets(self):
        self.assertEqual(mp.Tree("(3)").evaluate(), 3.0)
        self.assertEqual(mp.Tree("(d1)").evaluate(), 1)
        self.assertEqual(mp.Tree("(d1+3)").evaluate(), 4)
        self.assertEqual(mp.Tree("(3d1)").evaluate(), 3)

    def test_die(self):
        for i in range(1000):  # significant?
            val = mp.Tree("2*d20+10").evaluate()
            self.assertLessEqual(val, 50)
            self.assertLessEqual(12, val)

    def test_die_rolls(self):
        t = mp.Tree("d1")
        t.evaluate()
        self.assertEqual(t.get_dice_rolls(), [1])
        t = mp.Tree("d1d1d1")
        t.evaluate()
        self.assertEqual(t.get_dice_rolls(), [1, 1, 1])
