# OracleBot
# Copyright (C) 2017  Tom Godden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import oraclebot.db.add_sheet as fs
from tests.testing.discord_mocks import User
from oraclebot.db import database as d
import csv


class CharacterFromFileTest(unittest.TestCase):

    def test_db_creation(self):
        db = d.Database("sqlite:///:memory:")

    def test_add_from_sheet(self):
        file_path = "testing/data/char_sheet.csv"
        db = d.Database("sqlite:///:memory:")
        discord_user = User(display_name="name", id="1")
        db.add_user(discord_user)
        # Add to db
        with open(file_path) as datafile:
            char_id = fs.add_character_from_file(db, datafile, discord_user)
        # Get traits
        traits = db.get_character_traits(char_id)
        # Check traits
        with open(file_path, newline="") as csvfile:
            reader = csv.DictReader(csvfile)
            readerdict = {row["Trait"].lower(): row["Value"] for row in reader}
            self.assertDictEqual(readerdict, traits)


class ItemFromFileTest(unittest.TestCase):
    def test_add_from_sheet(self):
        file_path = "testing/data/item_sheet1.csv"
        db = d.Database("sqlite:///:memory:")
        # Add to db
        with open(file_path) as datafile:
            item_id = fs.add_item_from_file(db, datafile, None)
        # Get traits
        traits = db.get_item_traits(item_id)
        # Check traits
        with open(file_path, newline="") as csvfile:
            reader = csv.DictReader(csvfile)
            readerdict = {row["Trait"].lower(): row["Value"] for row in reader}
            self.assertDictEqual(readerdict, traits)
