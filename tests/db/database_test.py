# OracleBot
# Copyright (C) 2017  Tom Godden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from oraclebot.db import database as d
from tests.testing.discord_mocks import User


class DatabaseTest(unittest.TestCase):
    def test_empty(self):
        db = d.Database("sqlite:///:memory:")
        self.assertEqual(db.get_all_users(), [])

    def test_user_table(self):
        db = d.Database("sqlite:///:memory:")
        username = "name"
        id = "1"
        discord_user = User(display_name=username, id=id)

        # add user
        db.add_user(discord_user)
        user = db.get_user(str(id))
        self.assertEqual(user["name"], username)
        self.assertEqual(len(db.get_all_users()), 1)

        for u in db.get_all_users():
            self.assertEqual(user["name"], u["name"])
            self.assertEqual(user["id"], u["id"])

        # add duplicate user
        db.add_user(discord_user)
        user = db.get_user(id)
        self.assertEqual(user["name"], username)
        self.assertEqual(len(db.get_all_users()), 1)

        # add other user
        username2 = "other name"
        id2 = "2"
        discord_user2 = User(display_name=username2, id=id2)
        db.add_user(discord_user2)

        user2 = db.get_user(id2)
        self.assertEqual(user2["name"], username2)
        self.assertEqual(len(db.get_all_users()), 2)

        # test for non-existing user
        self.assertIsNone(db.get_user("123456789"))  # ID does not exist

    def test_character_table(self):
        db = d.Database("sqlite:///:memory:")
        user1 = User(display_name="user1", id="1")
        db.add_user(user1)
        # add character
        db.add_character(user1)
        self.assertEqual(len(db.get_characters()), 1)
        # add character by same user
        db.add_character(user1)
        self.assertEqual(len(db.get_characters()), 2)
        # get characters by user
        self.assertEqual(len(db.get_characters(user1)), 2)
        # add a character for a second user
        user2 = User(display_name="user2", id="2")
        db.add_user(user2)
        db.add_character(user2)
        self.assertEqual(len(db.get_characters()), 3)
        self.assertEqual(len(db.get_characters(user1)), 2)
        self.assertEqual(len(db.get_characters(user2)), 1)

    def test_traits(self):
        db = d.Database("sqlite:///:memory:")
        # one trait
        trait_name = "trait1"
        db.add_new_trait(trait_name)
        self.assertEqual(db.get_trait(trait_name)["name"], trait_name)
        # add second trait
        trait2_name = "trait2"
        db.add_new_trait(trait2_name)
        self.assertEqual(db.get_trait(trait2_name)["name"], trait2_name)
        # add trait a second time
        db.add_new_trait(trait_name)  # should NOT give an error

        # create character
        user1 = User(display_name="user1", id="1")
        db.add_user(user1)
        char_id = db.add_character(user1)
        # add trait
        val1 = "val1"
        db.add_trait_to_char(trait_name, char_id, val1)
        self.assertDictEqual(db.get_character_traits(char_id),
                             {trait_name: val1})
        # add another trait
        val2 = "val2"
        db.add_trait_to_char(trait2_name, char_id, val2)
        self.assertDictEqual(db.get_character_traits(char_id),
                             {trait_name: val1, trait2_name: val2})

        # add another character
        char_id2 = db.add_character(user1)
        # check traits
        self.assertDictEqual(db.get_character_traits(char_id2), {})
        # add a trait
        val3 = "val3"
        db.add_trait_to_char(trait2_name, char_id2, val3)
        self.assertDictEqual(db.get_character_traits(
            char_id2), {trait2_name: val3})
        # check if it didn't influence other characters
        self.assertDictEqual(db.get_character_traits(char_id),
                             {trait_name: val1, trait2_name: val2})

    def test_items(self):
        db = d.Database("sqlite:///:memory:")
        # create item
        item1 = db.add_item()
        self.assertDictEqual(db.get_item_traits(item1), {})
        # add a trait to the item
        db.add_trait_to_item(item1, "trait1", "val1")
        self.assertDictEqual(db.get_item_traits(item1), {"trait1": "val1"})
        # add another trait
        db.add_trait_to_item(item1, "trait1.2", "val1.2")
        self.assertDictEqual(db.get_item_traits(item1), {"trait1": "val1",
                                                         "trait1.2": "val1.2"})

        # create another item, instantiating it with traits
        item2 = db.add_item(
            item_traits={"trait2": "val2", "trait2.2": "val2.2"})
        self.assertNotEqual(item1, item2)
        self.assertDictEqual(db.get_item_traits(item1), {"trait1": "val1",
                                                         "trait1.2": "val1.2"})
        self.assertDictEqual(db.get_item_traits(item2), {"trait2": "val2",
                                                         "trait2.2": "val2.2"})

        # empty item starts empy
        item3 = db.add_item()
        self.assertNotEqual(item1, item3)
        self.assertDictEqual(db.get_item_traits(item3), {})

        # assign item to a character
        user1 = User(display_name="user1", id="1")
        char_id = db.add_character(user1)
        db.assign_item(item1, char_id)
        self.assertListEqual(db.get_character_item_ids(char_id), [item1])
        # create a new item and assign it during construction
        item4 = db.add_item(character_id=char_id)
        self.assertListEqual(
            db.get_character_item_ids(char_id), [item1, item4])
        # unassign an item
        db.assign_item(item1, None)
        self.assertListEqual(db.get_character_item_ids(char_id), [item4])
        db.assign_item(item4, None)
        self.assertListEqual(db.get_character_item_ids(char_id), [])
