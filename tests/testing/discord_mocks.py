# OracleBot
# Copyright (C) 2017  Tom Godden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import asyncio


class ClientException(Exception):
    pass


class Role():
    def __init__(self, name=None):
        self.name = name


class User():
    def __init__(self, display_name=None, id=None, name=None):
        self.name = name
        self.display_name = display_name
        self.id = id


class Member(User):
    def __init__(self, display_name=None, id=None, name=None, roles=None):
        super(Member, self).__init__(display_name, id, name)
        self.roles = roles
    pass


class Server():
    def __init__(self, members=None, roles=None, default_channel=None):
        self.members = members
        self.roles = roles
        self.default_channel = default_channel

    def get_member_named(self, name):
        for m in self.members:
            if m.display_name == name:
                return m


class Channel():
    def __init__(self, id=None, channel_type=None, server=None):
        self.id = id
        self.type = channel_type
        self.server = server
        self.log = []
    pass

    async def send(self, message):
        self.log += [message]



class Message():
    def __init__(self, content=None, channel=None, author=None):
        self.content = content
        self.channel = channel
        self.author = author


class _LogEntry():
    pass


class _RunEntry(_LogEntry):
    pass


class _MessageEntry(_LogEntry):
    def __init__(self, channel, text):
        self.channel = channel
        self.text = text


class _ReplaceRolesEntry(_LogEntry):
    def __init__(self, member, new_roles):
        self.member = member
        self.role_list = new_roles


class _GetAllChannelsEntry(_LogEntry):
    pass


class Client():
    def __init__(self, user=None):
        self.user = user
        self.log = []

    def event(self, coro):
        # Code directly from source at:
        # https://github.com/Rapptz/discord.py/blob/async/discord/client.py
        if not asyncio.iscoroutinefunction(coro):
            raise ClientException(
                'event registered must be a coroutine function')
        setattr(self, coro.__name__, coro)
        return coro

    def run(self):
        self.log.append(_RunEntry())

    async def send_message(self, channel, text):
        self.log.append(_MessageEntry(channel, text))

    async def replace_roles(member, *new_roles):
        self.log.append(_ReplaceRolesEntry(member, new_roles))

    async def get_all_channels(self):
        self.log.append(_GetAllChannelsEntry())
        return []


class EventTriggerer():
    def __init__(self, client):
        self.client = client
        self.loop = asyncio.get_event_loop()

    def trigger_on_ready(self):
        try:
            self.loop.run_until_complete(client.on_ready(message))
        except AttributeError:
            pass

    def trigger_message(self, message):
        try:
            self.loop.run_until_complete(client.on_message(message))
        except AttributeError:
            pass

    def trigger_event(self, before, after):
        try:
            self.loop.run_until_complete(
                client.on_member_update(before, after))
        except AttributeError:
            pass


if __name__ == "__main__":
    client = Client(user=None)
    channel = Channel(None, None, None)

    @client.event
    async def on_message(message):
        print(message.content)
    triggerer = EventTriggerer(client)
    client.run()
    triggerer.trigger_message(Message("test", channel))
