# OracleBot
# Copyright (C) 2017  Tom Godden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import unittest
import asyncio
import tests.testing.discord_mocks as mocks
import oraclebot.common.strings as strings
import oraclebot.oracle_bot_commands as cmd

noargs = []
user1 = mocks.User(display_name="user1",
                   id="1",
                   name="user1")
empty_message = mocks.Message(content="", channel=None, author=None)


def new_empty_message_with_channel(channel):
    return mocks.Message(content="", channel=channel, author=None)


class CommandsTest(unittest.TestCase):
    def setUp(self):
        self.channel1 = mocks.Channel()
        self.loop = asyncio.get_event_loop()

    def test_source_command(self):
        async def test():
            await cmd.source_command_f(None, new_empty_message_with_channel(self.channel1), None, noargs)
            self.assertIsInstance(self.channel1.log[0], str)
            self.assertEqual(self.channel1.log[0], strings.source_string())

        self.loop.run_until_complete(test())

    def test_d_command(self):
        async def test():
            # 1000 d20s should be sufficient
            for i in range(1000):
                await cmd.d_command_f(None, new_empty_message_with_channel(self.channel1), None, [20])
                top = self.channel1.log[-1]
                self.assertIsInstance(top, int)
                self.assertGreaterEqual(20, top)
                self.assertGreaterEqual(top, 0)

        self.loop.run_until_complete(test())
